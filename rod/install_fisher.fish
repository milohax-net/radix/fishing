set --query fisher_path;
  or set --universal --export fisher_path $LIB/fisher

  echo fisher_path: $fisher_path

# See https://github.com/jorgebucaran/fisher
curl -sL https://raw.githubusercontent.com/jorgebucaran/fisher/main/functions/fisher.fish \
  | source && fisher install jorgebucaran/fisher
# Now install my plugins
pushd (realpath $__fish_config_dir)
  git restore fish_plugins
popd
fisher update
# exec fish
