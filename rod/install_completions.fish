#!/usr/bin/env fish
# Re-install fish shell completions and integrations
test -e $ASDF_DIR/compeltions/asdf.fish
  and ln -f -s $ASDF_DIR/compeltions/asdf.fish \
    $__fish_config_dir/completions/asdf.fish
test -e $LIB/yadm/completion/fish/yadm.fish
  and ln -f -s $LIB/yadm/completion/fish/yadm.fish \
    $__fish_config_dir/completions/yadm.fish
if is_exe pipx
  if pipx list|grep virtualfish
    pipx reinstall virtualfish
  else
    pipx install virtualfish
  end
  vf install
end
if is_exe broot
  echo -n "broot: "
  broot --install
end
