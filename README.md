# fishing

Fish shell functions and scripts

## Installing

Clone this to the fish configuration location.

Note: I like to place my fishing kit in `~/lib/fish`, so I will clone
there, and then symlink it to fish's `$__fish_config_dir`

```shell
git clone git@gitlab.com:milohax-net/radix/fishing.git ~/lib/fish
rm -rf $__fish_config_dir
ln -s ~/lib/fish $__fish_config_dir
```
## Integrations

### Completions

- ASDF: `ln -s $ASDF_DIR/compeltions/asdf.fish $__fish_config_dir/completions/asdf.fish`
- yadm: `ln -s ~/lib/yadm/completion/fish/yadm.fish ~/lib/fish/completions/yadm.fish`

### Broot

Run `broot --install` to re-install `functions/br.fish`

### Virtual fish

```
pipx reinstall virtualfish
vf install

