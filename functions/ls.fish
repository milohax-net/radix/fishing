function ls --wraps=lsd --description 'List files, with a lot of pretty colors and some other stuff'
  lsd --group-dirs first $argv;
end
