function fwd --wraps=nextd --description 'Change to next directory in history'
  nextd $argv; 
end
