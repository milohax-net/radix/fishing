function fish_greeting --description 'Print a welcome banner when fish starts'

    # if there is a $fish_greeting variable aleady defined, then use that,
    # otherwise, compose one now using some system details and the fish version
    if not set --query fish_greeting
        set line0 \n(printf 'This is %s, %s' (hostname -s) (uname -srm))
        set line1 \n(printf '%s\n\n' (fish --version))
        set line2 \n(printf (_ 'Type %shelp%s for hypertext manual.') (set_color cyan) (set_color normal))
        set --global fish_greeting "$line0$line1$line2"
    end

    if set --query fish_private_mode
        set --local line (_ "fish is running in private mode, history will not be persisted.")
        set --global fish_greeting $line
    end

    # The greeting used to be skipped when fish_greeting was empty (not just
    # undefined) keep it that way to not print superfluous newlines on old
    # configuration
    if test -n "$fish_greeting"
        set --local linecount (echo $fish_greeting|count)
        if test $linecount -ge 3  # we have our fancy header
            if type --quiet lolcat
                echo $fish_greeting | head -3 | lolcat -a -s 15 -d 3 -F 0.4
            else
                echo $fish_greeting | head -3
            end
            echo $fish_greeting | tail -n (math $linecount - 3)
            echo
        else
            echo $fish_greeting
        end
    end
end
