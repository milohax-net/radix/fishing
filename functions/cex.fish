# Defined interactively
function cex --wraps=command --description 'Execute Conda-managed command.'
    if not set --query CONDA_PREFIX
        conda activate
        set __cex_set 1
    end
    command $argv
    if set --query __cex_set
        conda deactivate
    end
end
