function mossh --wraps=mosh --description 'mossh remote shell'
  mosh $argv
end
