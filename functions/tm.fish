function tm --wraps=tmux --description 'alias tmux'
  tmux $argv
end
