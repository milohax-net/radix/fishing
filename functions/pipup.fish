function pipup --wraps='pip install --upgrade pip' --description 'Upgrade pip'
  pip install --upgrade pip $argv; 
end
