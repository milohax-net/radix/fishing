function wtfo --wraps=wtf --description 'Look up an abbreviation, including obscene meanings'
   string replace 'nothing appropriate' 'nothing inappropriate' (wtf -o $argv)
end
