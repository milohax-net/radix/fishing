# Defined interactively
function bex --wraps='bundle exec' --description 'Execute Bundle-managed command.'
    bundle exec $argv
end
