function __milo_set_path --description "Set the fish PATH how I like"
  # ditry PATH hax
  #
  set --erase fish_user_paths
  fish_add_path \
    $HOME/bin \
    $HOME/Work/bin \
    $HOME/.rd/bin \
    $CARGO_HOME/bin \
    $GOPATH/bin \
    $BASHING/bin \
    /usr/local/bin \
    /usr/local/sbin \
  is_osx; and is_arm; and fish_add_path --append /opt/homebrew/bin
  contains '.' $PATH; or set --export PATH $PATH '.'
end
