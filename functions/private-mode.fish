# Defined interactively
function private-mode --description 'Start a private-mode fish session.'
  set --export fish_private_mode t
    fish
  set --erase fish_private_mode
end
