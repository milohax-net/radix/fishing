function tmcons --wraps='tmuxp load ~/etc/tmuxp/console.yaml' --description 'alias tmcons=tmuxp load ~/etc/tmuxp/console.yaml'
  tmuxp load ~/etc/tmuxp/console.yaml $argv
        
end
