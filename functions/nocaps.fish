function nocaps --description "Convert macOS clipboard content to lower case"
  pbpaste | tr '[:upper:]' '[:lower:]' | pbcopy $argv
end
