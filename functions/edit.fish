function edit --wraps code --description 'Run VSCode editor with default profile'
  if is_exe code
    if test $TERM_PROGRAM = 'vscode'
      code $argv
    else
      code --new-window --profile Default $argv
    end
  else #fallback
    eval $EDITOR $argv
  end
end
