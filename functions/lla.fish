function lla --wraps=ls --description 'List contents of directory using long format, including hidden files'
    ls -lhA $argv
end