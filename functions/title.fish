function title --description 'set terminal title'
# This sets the terminal window title on X-11 terminal emulators
  echo -ne "\033]0;$argv\007"
end
