# Defined interactively
function dex --wraps='direnv exec' --description 'Execute Direnv-managed command.'
    direnv exec $argv
end
