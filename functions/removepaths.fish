# See https://superuser.com/a/1212305/161668
# Note: that answer recommends not to use PATH, but I haven't looked into why
#       not, and I am using elsewhere PATH, so I've had to use it here.
function removepaths --description "Remove paths from global fish_user_paths"
  if set -l index (contains -i $argv[1] $PATH)
#      set --erase --universal fish_user_paths[$index]
      set --erase --universal PATH[$index]
      echo "Updated PATH: $PATH"
  else
      echo "$argv[1] not found in PATH: $PATH"
  end
end