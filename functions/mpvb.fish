function mpvb --wraps='mpv --demuxer-readahead-secs=12' --description 'run mpv with a 12-second read-ahead buffer'
  # reduces "(buffering)" when streaming a file over the Net
  mpv --demuxer-readahead-secs=12 $argv
end
