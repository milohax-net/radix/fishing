function rsyncdz --wraps rsyncd --description="remote/recursive sync directories using rsync with compression, properly"
  # see https://gitlab.com/-/snippets/2183995
  rsyncd --compress $argv
end