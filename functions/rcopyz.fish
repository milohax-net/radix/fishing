function rcopyz --wraps='rsync' --description 'recursive copy using rsync, with compression'
  # see https://gitlab.com/-/snippets/2183995
  rcopy --compress $argv; 
end
