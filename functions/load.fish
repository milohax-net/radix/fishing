function load --description "Load (source) file(s) into the shell environment."

# Only loads if the file is present. Silently ignores missing files.

  if not set --query argv[1]
        error (status current-command)": must specify a file to load";
        usage (status current-command) "<file> [<file>]..."
        return 1;
  end

  set --local MODULE
  for MODULE in $argv
    test -f $MODULE; or test -s $MODULE
      and source $MODULE
  end
end
