function allcaps --description "Convert macOS clipboard content to UPPER CASE"
  pbpaste | tr '[:lower:]' '[:upper:]' | pbcopy $argv
end
