function rcopy --wraps='rsync' --description 'recursive copy using rsync'
  # see https://gitlab.com/-/snippets/2183995
  rsync --hard-links --partial --progress --verbose --archive $argv; 
end
