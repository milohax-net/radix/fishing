function d --wraps=lsd --description 'list directories only (not files)'
  tree --dirsfirst -aLpughDFiC 1 -d $argv;
end
