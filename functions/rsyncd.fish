function rsyncd --wraps='rsync' --description="remote/recursive sync directories using rsync, properly"
  # see https://gitlab.com/-/snippets/2183995
  # Note that the args must end in / to work as expected

  set --local RC 0 # return code
rc
  if [ count $argv -lt 2 ]
    error "Must supply source and destination directories"
    set RC 1
  end

  set --local SOURCE $argv[1]
  set --local DEST $argv[2]

  if not test -d $SOURCE
    or not test -d $DEST
    error "Source and Destination must be directories"
    set RC 2
  end
  if [ $RC -ne 0 ]
    usage (status curent-command)": <source-dir>/ <dest-dir>/"
    return $RC
  end

  rcopy --delete $SOURCE/ $DEST/
end
