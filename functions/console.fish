function console --wraps='~/etc/aerospace/console.sh' --description 'Launch and attach macOS console'
  # This is a convenience to start or attach to the console from the current
  # terminal. Useful if the console window was closed, so that the AeroSpace
  # binding has nothing to pop up.
  is_osx; and ~/etc/aerospace/console.sh $argv
end
