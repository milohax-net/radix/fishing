function __load_env --description "Load a POSIX enviroment file"

# Based on http://lewandowski.io/2016/10/fish-env/
# modified to skip comments, use Fish's own string manip,
# and handle whitespaces around the = sign.

# Limitations:
# - Property keys must begin with a letter in the first
#   character of the line
#
  for i in (cat $argv | string match --regex '^[A-Za-z0-9_].*')
    set prop (string replace '~' $HOME \
      (string trim (string split --max 1 = $i)))
    set --global --export $prop[1] (string trim --chars \" $prop[2])
  end
end
