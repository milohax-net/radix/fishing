# Defined interactively
function aex --wraps='asdf exec' --description 'Execute ASDF-managed command.'
    asdf exec $argv
end
