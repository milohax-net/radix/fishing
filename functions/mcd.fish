function mcd --wraps=mkdir --description 'Make directory and change into it'
#Make multiple directories at once, CD into the last one
  if not set --query argv[1]
    usage (status current-command)": <directory> [<directory> ...]"
    return 1
  end
  mkdir -p $argv
  if test -d $argv[-1]
    cd $argv[-1]
  end
end
