function back --wraps=prevd --description 'Change to previous directory in history'
  prevd $argv; 
end
