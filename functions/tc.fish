function tc --description 'tmux attach console session'
  # Not wrapping tmux, because the command completions don't make sense
  # for attaching sessions.
  # TODO: use resurrect or similar to persist the console
  tmux attach-session -t console || tmux new-session -s console
end
