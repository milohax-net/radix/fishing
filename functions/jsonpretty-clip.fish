# Defined in /Users/mjl/tmp/fish.s8ufwI/jsonpretty-clip.fish @ line 2
function jsonpretty-clip --wraps=pbpaste\|jq\ \'.\'\|pbcopy --description 'prettyfy JSON from the clipboard'
  pbpaste|jq '.'|pbcopy
end
