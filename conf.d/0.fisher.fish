if test -z $fisher_path
  __load_env ~/etc/env/{*.env,*.properties}
  source ~/lib/fish/rod/install_fisher.fish
  source ~/lib/fish/rod/install_completions.fish
  exec fish
end

# Taken from https://github.com/kidonng/fisher_path.fish

# Storing this locally still: how would you bootstrap this as a plugin?

# To stop loading plugins in new sessions, create a variable
# $_fisher_path_initialized via set --universal
# _fisher_path_initialized. To revert, just erase it via set --erase
# _fisher_path_initialized.

set --query _fisher_path_initialized && exit
set --global _fisher_path_initialized

if test -z "$fisher_path"; or test "$fisher_path" = "$__fish_config_dir"
  exit
end

set fish_complete_path $fish_complete_path[1] $fisher_path/completions $fish_complete_path[2..]
set fish_function_path $fish_function_path[1] $fisher_path/functions $fish_function_path[2..]

for file in $fisher_path/conf.d/*.fish
  if ! test -f (string replace --regex "^.*/" $__fish_config_dir/conf.d/ -- $file)
    and test -f $file && test -r $file
    source $file
  end
end
