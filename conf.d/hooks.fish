# This is slow, so only load it if asked for by touching ~/.fish_hooks
status --is-interactive; and test -f ~/.fish_hooks; or exit

time begin
  log (notice LOADING HOOKS)
  if test -f /opt/anaconda3/bin/conda
    eval /opt/anaconda3/bin/conda "shell.fish" "hook" $argv | source
    conda deactivate
  end
  if is_exe direnv
    direnv hook fish|source
  end
end
