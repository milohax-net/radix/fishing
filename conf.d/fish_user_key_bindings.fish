function fish_user_key_bindings --description 'binds keys to commands in fish'
  if not is_exe fzf_configure_bindings
    # Fuzzyfind is better, but if we don't have it, these are still good
    bind \cf 'echo; ls -l; commandline -f repaint'
    bind \cg 'git diff; commandline -f repaint'
    bind \cs 'git status; commandline -f repaint'
    bind \cp 'ps auww|eval $PAGER; jobs; commandline -f repaint'
  end
  #TODO: Work out key bindings
  #  Default seem to have gone?
  #   - Alt-p  pager
  #   - Alt-s  sudo
  #   - Alt-←  prevd  (works outside tmux, maybe change to Alt-,)
  #   - Alt-→  nextd  (works outside tmux, maybe change to Alt-.)
  #   - Alt-e  eval $EDITOR
end
