status --is-interactive; or exit
is_exe fzf; or exit

if is_exe fzf_configure_bindings
  # Configure key bindings for fzf plugin
  # See fzf_configure_bindings --help
  fzf_configure_bindings \
    --directory=\cf \
    --git_log=\cg \
    --git_status=\cs \
    --processes=\cp
  set fzf_directory_opts --bind "ctrl-o:execute($EDITOR {} &> /dev/tty)"
else
  set --local CONFIG (status basename)
  set --local ROD (string replace '//' '/' \
    (find $__fish_config_dir/ -name 'inst*fisher*'))
  test -n "$ROD"; and warn "$CONFIG: fzf plugin is not installed. Run $ROD"
end
