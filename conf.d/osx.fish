is_osx; or exit

set --query OSX_CONFIG
  or set --universal --export OSX_CONFIG "$HOME/Library/Application Support"
