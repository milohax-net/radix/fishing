status --is-interactive; or exit

# Prompt and shell integration
string match --quiet "$TERM_PROGRAM" "iTerm.app"
  and load ~/.iterm2_shell_integration.fish
string match --quiet "$TERM_PROGRAM" "vscode"
  and load (code --locate-shell-integration-path fish)
set --export STARSHIP_CONFIG ~/etc/starship/fish.toml
# is_exe starship; and starship init fish | source
## https://github.com/justinmayer/virtualfish/issues/182
#set VIRTUAL_ENV_DISABLE_PROMPT true  #using Tide for this
