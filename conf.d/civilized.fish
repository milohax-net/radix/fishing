status --is-interactive; or exit

# These things make my shell life more livable
is_exe thefuck; and thefuck --alias | source  #Fix up command mistakes
is_exe bat; and begin
  set --export PAGER "bat --plain"
  set --export MANPAGER "sh -c 'col -bx | bat -l man -p'"
end

# Create home base directories
#
# The actual pathnames come from environment variables, which should
# have been set before running this script. My Dotfiles project sets the
# ones that I like, in .config/env/places.env ($ETC/env/places.env)

# Assuming that variables are all set by now (see _environment.fish)
if contains (hostname) (string split " " $WORKSTATIONS)
  for d in \
    $BAK $BIN $FUN \
    $HAX $IMG $KEY \
    $LAB $LIB $NET \
    $SRV $TMP $VMS
    test -e $d ; or mkdir -p $d
  end
else
  for d in \
    $BAK $BIN \
    $HAX $LAB \
    $LIB $TMP ; do
    test -e $d ; or mkdir -p $d
  end
  test -e ~/var ; or ln -s $VAR ~/var
  test -e ~/etc ; or ln -s $CONFIG ~/etc
end

if contains (hostname) (string split " " $GITLAB_WORKSTATION)
  # workaround "security" scanner's SSL fudge-up
  set --global --export GIT_SSL_NO_VERIFY 1
end
